module.exports = {
  id: 1,
  name: 'Final Fantasy XV',
  coverImg: '/v1483367511/final-fantasy-15-cover.jpg',
  priceRange: [34.99, 59.99],
  platforms: [
    {
      id: 1,
      abbreviation: 'PS4',
      name: 'PlayStation 4'
    },
    {
      id: 2,
      abbreviation: 'XONE',
      name: 'Xbox One'
    }
  ],
  releaseDate: '2016-11-29T06:00:00Z',
  pricing: {
    sort: {
      name: 'bestPrice',
      direction: 'desc'
    },
    filters: {
      medium: ['digital'],
      platform: [1]
    },
    data: [
      {
        vendor: 'Amazon',
        platform: 'PlayStation 4',
        client: null,
        medium: 'digital',
        currentPrice: {
          date: '2017-03-13T00:06:00Z',
          price: 34.99
        },
        previousPrice: {
          date: '2017-03-12T00:06:00Z',
          price: 59.99
        },
        url: 'https://www.amazon.com/Final-Fantasy-XV-PlayStation-4/dp/B00DBF829C?th=1'
      },
      {
        vendor: 'Best Buy',
        platform: 'PlayStation 4',
        client: null,
        medium: 'digital',
        currentPrice: {
          date: '2017-03-13T00:06:00Z',
          price: 59.99
        },
        previousPrice: null,
        url: 'http://www.bestbuy.com/site/final-fantasy-xv-day-one-edition-playstation-4/9497126.p?skuId=9497126'
      },
      {
        vendor: 'GameStop',
        platform: 'PlayStation 4',
        client: null,
        medium: 'digital',
        currentPrice: {
          date: '2017-03-13T00:06:00Z',
          price: 59.99
        },
        previousPrice: null,
        url: 'http://www.gamestop.com/ps4/games/final-fantasy-xv/125318'
      },
      {
        vendor: 'PlayStation Store',
        platform: 'PlayStation 4',
        client: null,
        medium: 'digital',
        currentPrice: {
          date: '2017-03-13T00:06:00Z',
          price: 59.99
        },
        previousPrice: null,
        url: 'https://store.playstation.com/#!/en-us/games/final-fantasy-xv/cid=UP0082-CUSA01633_00-FINALFANTASYXV00'
      },
    ]
  }
};