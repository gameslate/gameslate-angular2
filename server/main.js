const path = require('path');

function getResource(p) {
  return require('./' + p);
}

require('bogus-api').create({
  resourceDir: path.resolve(__dirname),
  resourceUriPrefix: '/api/v1',

  priorityRoutes: function(server) {

    server.get('/api/v1/games/:id', function(req, res) {
      res.status(200).json(getResource(`games/${req.params.id}`));
    });

  }
}).start();