import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';

import './sass/main.scss';
import AppComponent from './app.component';
import { RoutingModule } from "./core/routing.module";
import { GamesService } from "./core/services/games.service";
import { AuthenticationService } from "./core/services/authentication.service";
import { ComponentsModule } from "./components/components.module";



@NgModule({
  imports: [
    BrowserModule,
    RoutingModule,
    ComponentsModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    GamesService,
    AuthenticationService
  ],
  bootstrap: [ AppComponent ]
})
export default class AppModule { }