import { Component, EventEmitter } from "@angular/core";
import { GamesService } from "../../../core/services/games.service";
import { Game } from "../../../core/interfaces/game.interface";

@Component({
  template: require('./game-pricing.template.html')
})
export class GamePricingComponent {
  constructor(private gameService: GamesService) {}

  game: Game;
  gameSubscription: EventEmitter<Game>;

  ngOnInit() {
    // try to get a saved game from the service
    this.game = this.gameService.getSavedGame();

    // if there's no game, then wait for it to be available
    if (!this.game) {
      this.gameSubscription = this.gameService.gameChange.subscribe(
          game => this.game = game
      )
    }
  }

  ngOnDestroy() {
    if (this.gameSubscription) {
      this.gameSubscription.unsubscribe();
    }
  }
}