import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { GamesService } from "../../core/services/games.service";
import { Game } from "../../core/interfaces/game.interface";
import { CloudinaryPipe } from "../../core/pipes/cloudinary.pipe";

const mapSvg = require('../../assets/map.svg');


@Component({
  template: `
<div class="page-error" *ngIf="error">
    <div class="page-error__img">${mapSvg}</div>
    <div class="page-error__title">
        Oh Bummer.
    </div>
    <div class="page-error__description">
        The game you're looking for doesn't exist.
    </div>
    <div>
        Try doing a search or go back to the <a [routerLink]="['/']">home page</a>.
    </div>
</div>

<div class="game" *ngIf="game && !error">
    <div class="container pt-xl">
        <div class="game-header">
            <div class="game-header__cover" [style.backgroundImage]="'url('+coverImg+')'"></div>
            <div class="game-header__info">
                <div class="game__notification">
                    <button class="btn btn--accent btn--large">
                        Set Your Own Price
                    </button>
                </div>
                <div class="game__name">{{game.name}}</div>
                <div class="game__price-range">&#36;{{game.priceRange[0]}} - &#36;{{game.priceRange[1]}}</div>
                <div class="game-header__tabs tabs">
                    <button class="tabs__btn" [routerLink]="['/games', game.id, 'pricing']" [routerLinkActive]="['active']">Pricing</button>
                    <button class="tabs__btn" [routerLink]="['/games', game.id, 'history']" [routerLinkActive]="['active']">History</button>
                </div>
            </div>
        </div>
    </div>
</div>
<router-outlet></router-outlet>
`
})
export class GameComponent {

  game: Game;
  coverImg: string;
  error: boolean = false;
  cloudinaryPipe = new CloudinaryPipe();

  constructor(private router: Router,
              private route: ActivatedRoute,
              private gamesService: GamesService
  ) {}

  ngOnInit() {
    this.route.params.flatMap(
        params => {
          return this.gamesService.getGame(params['gameId']);
        }
    ).subscribe(
        next => {
          this.coverImg = this.cloudinaryPipe.transform(next.coverImg, 100);
          this.game = next;
        },
        err => this.error = true
    );
  }
}