import { Component, ViewChild } from "@angular/core";
import { GamesService } from "../../core/services/games.service";
import { Game } from "../../core/interfaces/game.interface";

@Component({
  template: `
<div class="tabs mt-lg">
    <button (click)="getPopular()"
       [class.active]="state=='popular'">
       <span>Popular</span>
    </button>
    <button (click)="getRecent()"
       [class.active]="state=='recent'">
       <span>Recent Releases</span>
    </button>
    <button (click)="getTop()"
       [class.active]="state=='top'">
       <span>Top Scores</span>
    </button>
</div>
<gs-grid [games]="games"></gs-grid>
`
})
export class HomeComponent {
  constructor(private gamesService: GamesService) {}

  @ViewChild('popularBtn') popularBtn;
  @ViewChild('recentBtn') recentBtn;
  @ViewChild('scoreBtn') scoreBtn;

  games: Game[] = [];
  state: string = 'popular';

  ngOnInit() {
    // this.getPopular();
  }

  changeState(id: string) {
    this.state = id;
  }

  // getPopular() {
  //   this.state = 'popular';
  //   this.gamesService.getGames().subscribe(
  //       res => this.games = res.docs
  //   );
  // }
  //
  // getRecent() {
  //   this.state = 'recent';
  //   this.gamesService.getGames().subscribe(
  //       res => this.games = res.docs
  //   );
  // }
  //
  // getTop() {
  //   this.state = 'top';
  //   this.gamesService.getGames().subscribe(
  //       res => this.games = res.docs
  //   );
  // }
}