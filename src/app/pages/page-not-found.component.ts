import { Component } from "@angular/core";
const mapSvg = require('../assets/map.svg');

@Component({
  template: `
<div class="page-error">
    <div class="page-error__img">${mapSvg}</div>
    <div class="page-error__title">
        Oh Bummer.
    </div>
    <div class="page-error__description">
        The page you're looking for doesn't exist.
    </div>
    <div>
        Try doing a search or go back to the <a [routerLink]="['/ewfdewd']">home page</a>.
    </div>
</div>
`
})
export class PageNotFoundComponent {}