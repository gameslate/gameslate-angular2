import { Component, Input } from "@angular/core";
import { CloudinaryPipe } from "../../core/pipes/cloudinary.pipe";
import { Game } from "../../core/interfaces/game.interface";

@Component({
  selector: 'gs-game-display',
  template: `
<a [routerLink]="['/game', game.uri]" class="game-display" [style.width]="this.size + 'px'">
    <div class="game-display__cover" [style.backgroundImage]="'url('+coverImgUrl+')'"></div>
    <div class="game-display__title">{{game.title}}</div>
</a>
`
})
export class GameDisplayComponent {

  @Input() game: Game;
  @Input() size: number = 120;

  cloudinaryPipe = new CloudinaryPipe();
  coverImg: string = '';

  ngOnInit() {
    this.coverImg = this.cloudinaryPipe.transform(this.game.coverImg, this.size);
  }
}