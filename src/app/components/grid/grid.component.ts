import { Component, Input, ViewChild, HostListener, QueryList, ViewChildren } from "@angular/core";
import { Game } from "../../core/interfaces/game.interface";
const _throttle = require('lodash/throttle');

@Component({
  selector: 'gs-grid',
  template: `
<div class="grid" #grid>
    <div class="grid__child" #gridChild>
        <gs-game-display *ngFor="let game of games" [game]="game" [size]="colWidth"></gs-game-display>
    </div>
</div>
`
})
export class GridComponent {
  @Input() games: Game[] = [];
  @ViewChild('grid') grid: any;
  @ViewChild('gridChild') gridChild: any;
  @HostListener('window:resize') throttleResize = _throttle(this.resize, 300);
  colWidth = 130;
  colGutter = 20;

  resize() {
    let columnCount = Math.floor((this.grid.nativeElement.clientWidth-this.colGutter)/(this.colWidth+this.colGutter));
    this.gridChild.nativeElement.style.width = `${((this.colWidth+this.colGutter)*columnCount)+this.colGutter}px`;

  }

  rules(width) {

    let index = 1;
    while (width < (this.colWidth * (1+index)) + (this.colGutter * index)) {

    }

    if (width < (this.colWidth * 2) + this.colGutter) {
      return 1;
    } else if (width < (this.colWidth * 3) + (this.colGutter * 2)) {
      return 2;
    } else if (width < (this.colWidth * 4) + (this.colGutter * 3)) {
      return 3;
    } else if (width < (this.colWidth * 5) + (this.colGutter * 4)) {
      return 4;
    } else if (width < (this.colWidth * 6) + (this.colGutter * 5)) {
      return 5;
    } else if (width < (this.colWidth * 7) + (this.colGutter * 6)) {
      return 6;
    } else if (width < (this.colWidth * 8) + (this.colGutter * 7)) {
      return 7;
    } else if (width < (this.colWidth * 9) + (this.colGutter * 8)) {
      return 8;
    } else if (width < (this.colWidth * 10) + (this.colGutter * 9)) {
      return 9;
    } else if (width < (this.colWidth * 11) + (this.colGutter * 10)) {
      return 10;
    }
  }

  resizeCards() {
    // _forEach(gridElements, (card, i) => {
    //   card[0].style.width = `${100/rules(currentParentWidth)}%`;
    // });
  }

  ngOnInit() {
    this.resize();
  }

}