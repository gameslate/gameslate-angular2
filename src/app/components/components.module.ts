import { NgModule } from "@angular/core";
import { GameDisplayComponent } from "./game-display/game-display.component";
import { RouterModule } from "@angular/router";
import { NavigationComponent } from "./navigation/navigation.component";
// import { IconModule } from "angular2-svg-icons";
import { GridComponent } from "./grid/grid.component";
import { CommonModule } from "@angular/common";

@NgModule({
  imports: [
    RouterModule,
    // IconModule,
    CommonModule
  ],
  declarations: [
    GameDisplayComponent,
    NavigationComponent,
    GridComponent,
  ],
  exports: [
    GameDisplayComponent,
    NavigationComponent,
    GridComponent
  ]
})
export class ComponentsModule {}

