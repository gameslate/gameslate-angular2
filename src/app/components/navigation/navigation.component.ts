import { Component } from "@angular/core";
const logoWhite = require('../../assets/logo-white-insignia.svg');

@Component({
  selector: 'gs-navigation',
  template: `
<div class="navigation-parent">
    <div class="container">
        <div class="navigation">
            <div class="navigation__logo" [routerLink]="['/']">${logoWhite}</div>
            <div class="navigation__search">
                <icon i="magnify"></icon>    
                <input type="text" placeholder="Search">
            </div>
            <div class="navigation__menu">
                <a class="navigation__link" [routerLink]="['/']" [routerLinkActive]="['active']" [routerLinkActiveOptions]="{exact: true}">
                    <icon i="home"></icon>
                </a>
                <a class="navigation__link" href="">
                    <icon i="account"></icon>
                </a>
            </div>
        </div>
    </div>
</div>
`
})
export class NavigationComponent {}