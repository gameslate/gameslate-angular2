import {
  Component, Directive, HostListener, Input,
  NgModule, EventEmitter, ElementRef
} from "@angular/core";


/**
 * dropdown component
 */
@Component({
  selector: 'tl-dropdown',
  template: `
<div class="dropdown">
    <ng-content select="[tlDropdownBtn]"></ng-content>    
    <ng-content select="[tlDropdownContent]"></ng-content>    
</div>
`
})
class TlDropdownComponent {
  @Input() tlDropdownStopPropagation: boolean = false;
  isOpen: boolean = false;
  dropdownEvent: EventEmitter<boolean> = new EventEmitter;
  clickHandlerRef = this.clickHandler.bind(this);

  constructor(private el: ElementRef) {
  }

  toggle() {
    if (!this.isOpen) {
      this.open();
    } else {
      this.close();
    }
  }

  open() {
    if (!this.isOpen) {
      this.isOpen = true;
      this.dropdownEvent.emit(this.isOpen);
      document.addEventListener('click', this.clickHandlerRef);
    }
  }

  close() {
    if (this.isOpen) {
      this.isOpen = false;
      this.dropdownEvent.emit(this.isOpen);
      document.removeEventListener('click', this.clickHandlerRef);
    }
  }

  isDescendantOfParent(child) {
    if (this.el.nativeElement == child) {
      return true;
    } else {
      let node = child.parentNode;
      while (node != null) {
        if (node == this.el.nativeElement) {
          return true;
        }
        node = node.parentNode;
      }
      return false;
    }
  }

  clickHandler(e) {
    if (!this.isDescendantOfParent(e.target)) {
      this.close();
    }
  }

  ngOnDestroy() {
    document.removeEventListener('click', this.clickHandlerRef);
  }
}


/**
 * Dropdown Button directive
 */
@Directive({
  selector: '[tlDropdownBtn]'
})
class TlDropdownBtnDirective {
  constructor(private tlDropdownComponent: TlDropdownComponent, el: ElementRef) {
    tlDropdownComponent.dropdownEvent.subscribe(
        next => el.nativeElement.classList.toggle('active', next)
    );
  }

  @HostListener('click', ['$event.target']) onClick() {
      this.tlDropdownComponent.toggle();
  }
}


/**
 * Dropdown content directive
 */
@Directive({
  selector: '[tlDropdownContent]'
})
class TlDropdownContentDirective {
  constructor(private tlDropdownComponent: TlDropdownComponent, el: ElementRef) {
    tlDropdownComponent.dropdownEvent.subscribe(
        next => el.nativeElement.classList.toggle('active', next)
    );
  }

  @HostListener('click', ['$event.target']) onClick() {
    if (!this.tlDropdownComponent.tlDropdownStopPropagation) {
      this.tlDropdownComponent.close();
    }
  }
}


/**
 * dropdown module
 */
@NgModule({
  declarations: [
    TlDropdownComponent,
    TlDropdownBtnDirective,
    TlDropdownContentDirective
  ],
  exports: [
    TlDropdownComponent,
    TlDropdownBtnDirective,
    TlDropdownContentDirective
  ]
})
export class TlDropdownModule {}