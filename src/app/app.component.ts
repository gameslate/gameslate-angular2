import { Component } from '@angular/core';
import { AuthenticationService } from "./core/services/authentication.service";
import { svgSymbols } from './components/icons/svg-symbols';

@Component({
  selector: 'app',
  template: `
${svgSymbols}
<div class="base">
    <gs-navigation></gs-navigation>
    <router-outlet></router-outlet>
</div>
`,

})
export default class {
  constructor(private authenticationService: AuthenticationService) {}

}