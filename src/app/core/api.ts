import { Response } from "@angular/http";
import { Observable } from "rxjs";

export const baseUrl = 'http://0.0.0.0:7001/api/v1';

export function unwrapResponse(res: Response) {
  return res.json();
}

export function handleObservableError(error: any) {
  let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} (${error.statusText})` : `Server Error`;
  console.error(`${errMsg} - Please file a bug report so we can get this fixed asap: https://gameslate.io/support`);
  return Observable.throw(errMsg);
}