import { Injectable, EventEmitter } from "@angular/core";
import { Observable } from "rxjs";
import { Game } from "../interfaces/game.interface";
import { Http, URLSearchParams } from "@angular/http";
import { baseUrl, handleObservableError, unwrapResponse } from '../api';

@Injectable()
export class GamesService {

  constructor(private http: Http) {}

  // getGames(search?:string): Observable<GameList> {
  //   let params: URLSearchParams = new URLSearchParams;
  //   if (search) {
  //     params.set('search', search);
  //   }
  //
  //   return this.http.get(`${baseUrl}/games`, {search: params})
  //       .map(unwrapResponse)
  //       .catch(handleObservableError);
  // }

  gameChange: EventEmitter<Game> = new EventEmitter;
  game: Game;

  emitGameChange(game: Game) {
    this.gameChange.emit(game);
  }

  getSavedGame() {
    return this.game;
  }

  getGame(uri:string): Observable<Game> {
    return this.http.get(`${baseUrl}/games/${uri}`)
        .map(data => {
          this.game = unwrapResponse(data);
          this.emitGameChange(this.game);
          return this.game;
        })
        .catch(handleObservableError);
  }
}