import { Injectable } from "@angular/core";

@Injectable()
export class LocalStorageService {

  storageName = 'gameslate';

  get(name) {
    return JSON.parse(window.localStorage.getItem(`${this.storageName}.${name}`));
  }

  set(name, value) {
    window.localStorage.setItem(`${this.storageName}.${name}`, JSON.stringify(value));
  }
}