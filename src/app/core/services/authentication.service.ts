import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Http, Response } from "@angular/http";
import { baseUrl, unwrapResponse, handleObservableError } from '../api';

@Injectable()
export class AuthenticationService {

  constructor(private http: Http) {}

  token;
  tokenExpires;

  authenticate(credentials): Observable<Response> {
    if (!credentials.username || !credentials.password) {
      throw Error(`[AuthenticationService] authenticate() credentials require username and password properties.`);
    }
    return this.http.post(`${baseUrl}/authenticate`, credentials)
        .map(unwrapResponse)
        .map(res => {
          if (res.token) {
            this.token = res.token;
            this.tokenExpires = res.expires;
          }
          return res;
        })
        .catch(handleObservableError)

  }

  getSession() {

  }

  validateSession() {

  }



}