import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'priceDiscount' })
export class PriceDiscountPipe implements PipeTransform {
  constructor() { }

  public transform(currentPrice, previousPrice) {
    if (currentPrice && previousPrice) {
      return `${100-(Math.round((currentPrice / previousPrice) * 100))}%`;
    } else {
      return '-';
    }
  }
}