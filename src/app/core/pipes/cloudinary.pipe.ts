import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'cloudinary' })
export class CloudinaryPipe implements PipeTransform {
  constructor() { }

  baseUrlHttp = 'http://res.cloudinary.com/game-slate/image/upload';
  baseUrlHttps = 'https://res.cloudinary.com/game-slate/image/upload';

  public transform(img, scaleWidth?) {
    if (img) {
      return `${this.baseUrlHttp}/w_${scaleWidth||100},c_scale${img}`;
    } else {
      return img;
    }
  }
}