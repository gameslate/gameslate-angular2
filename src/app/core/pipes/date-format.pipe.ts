import { Pipe, PipeTransform } from '@angular/core';
const moment = require('moment');

@Pipe({ name: 'dateFormat' })
export class DateFormat implements PipeTransform {
  constructor() { }

  public transform(date, format?) {
    if (date) {
      return moment(date).format('MMM Do, YYYY');
    } else {
      return date;
    }
  }
}