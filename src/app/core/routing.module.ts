import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { CloudinaryPipe } from "./pipes/cloudinary.pipe";
import { HttpModule } from "@angular/http";
import { DateFormat } from "./pipes/date-format.pipe";
import { ComponentsModule } from "../components/components.module";

import { HomeComponent } from "../pages/home/home.component";
import { GameComponent } from "../pages/game/game.component";
import { GamePricingComponent } from '../pages/game/pricing/game-pricing.component';
import { GameHistoryComponent } from '../pages/game/history/game-history.component';
import { PageNotFoundComponent } from "../pages/page-not-found.component";
import { PriceDiscountPipe } from "./pipes/price-discount.pipe";
import { TlDropdownModule } from '../components/dropdown/dropdown.component';

// import { IconModule } from "angular2-svg-icons";

const routes: Routes = [
  { path: 'games/:gameId', component: GameComponent, children: [
    { path: '', redirectTo: 'pricing', pathMatch: 'full' },
    { path: 'pricing', component: GamePricingComponent },
    { path: 'history', component: GameHistoryComponent },
  ]},
  { path: '', component: HomeComponent },
  { path: 'page-not-found', component: PageNotFoundComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule,
    HttpModule,
    ComponentsModule,
    // IconModule,
    TlDropdownModule
  ],
  declarations: [
    HomeComponent,
    PageNotFoundComponent,
    GameComponent,
    GamePricingComponent,
    GameHistoryComponent,
    CloudinaryPipe,
    DateFormat,
    PriceDiscountPipe
  ],
  exports: [
    RouterModule
  ]
})
export class RoutingModule {}