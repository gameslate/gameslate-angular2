interface Score {
  gameplay?: number;
  sound?: number;
  controls?: number;
  story?: number;
  graphics?: number;
  replayValue?: number;
}

export { Score };