interface Game {
  id: number;
  name: string;
  coverImg: string;
  priceRange: Array<number>;
  platforms: Platform[];
  releaseDate: Date;
  pricing: {
    sort: string;
    filters: Array<string>;
    data: GamePrice[];
  }
}

interface GamePrice {
  vender: Vendor;
  client: Client;
  currentPrice: GameHistoryItem;
  previousPrice: GameHistoryItem;
  medium: string;
  platform: string;
  url: string;
}

interface Platform {
  id: number;
  abbreviation: string;
  name: string;
}

interface Vendor {
  id: number;
  name: string;
  url: string;
}

interface Client {
  id: number;
  name: string;
}

interface GameHistory {
  vendor: Vendor;
  data: GameHistoryItem[]
}

interface GameHistoryItem {
  date: Date;
  price: Number;
}

interface Filter {
  medium?: Array<Medium>;
  platform?: Array<number>;
  vendor?: Array<number>;
  client?: Array<number>;
}

interface Sort {
  name: SortName;
  direction: SortDirection;
}

enum SortName {
  bestPrice,
  name
}

enum SortDirection {
  desc,
  asc
}

enum Medium {
  digital,
  physical
}

export {
  GameHistory,
  GameHistoryItem,
  Medium,
  SortDirection,
  SortName,
  Sort,
  Filter,
  Client,
  Vendor,
  Platform,
  GamePrice,
  Game
};